import './style/index.css';
import * as React from 'react';
import { injectable, inject, postConstruct } from 'inversify';
import { ApplicationShell, ReactWidget } from '@theia/core/lib/browser';
import { CommandRegistry } from '@theia/core/lib/common/command';
import { Message } from '@phosphor/messaging';

class InputView extends React.Component<{}, {}> {
  state = {
    name: "noName"
  };
  handleNameChange = (event: React.ChangeEvent<HTMLInputElement> | string) => {
    const name = typeof event === 'string' ? event : event.currentTarget.value;
    this.setState({
      name: name
    });
  }

  render(): React.ReactNode {
    return (
      <div>
        <input type="text" placeholder="your name" onChange={this.handleNameChange} />
        <div> {this.state.name} </div>
      </div>
    );
  }
}

@injectable()
export class NewWidget extends ReactWidget {

  protected static LABEL = 'New Widget';

  @inject(ApplicationShell) protected readonly shell: ApplicationShell;
  @inject(CommandRegistry) protected readonly commands: CommandRegistry;
  
  constructor() {
    super();
    this.node.tabIndex = 0;
    this.id = 'theia-newWidgetContainer';
    this.addClass('theia-new-widget');
    this.title.iconClass = 'scm-tab-icon';
    this.title.label = NewWidget.LABEL;
    this.title.caption = NewWidget.LABEL;
    this.title.closable = true;

  }

  @postConstruct()
  protected init(): void {
    this.refresh();
  }

  protected refresh(): void {
    this.update();
  }

  protected onActivateRequest(msg: Message): void {
    super.onActivateRequest(msg);
  }

  protected onAfterShow(msg: Message): void {
    super.onAfterShow(msg);
    this.update();
  }

  protected render(): React.ReactNode {
    return (
      <React.Fragment>
        <InputView />
      </React.Fragment>
    );
  }
}


