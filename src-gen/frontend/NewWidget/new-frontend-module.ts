import { ContainerModule } from 'inversify';
import { bindViewContribution, WidgetFactory, FrontendApplicationContribution } from '@theia/core/lib/browser';
import { TabBarToolbarContribution } from '@theia/core/lib/browser/shell/tab-bar-toolbar';
import { NewWidget } from './NewWidget';
import { NEW_WIDGET_FACTORY_ID, NewContrib } from './new-contrib';
import { NewMiniBrowserOpenHandler } from "./new-widget-mini-browser-open-handler";

export default new ContainerModule(bind => {
  bind(WidgetFactory).toDynamicValue(ctx => ({
    id: NEW_WIDGET_FACTORY_ID,
    createWidget: () => ctx.container.get(NewWidget)
  })).inSingletonScope();
  
  bindViewContribution(bind, NewContrib);
  
  bind(FrontendApplicationContribution).toService(NewContrib);
  bind(NewWidget).toSelf().inSingletonScope();

  bind(TabBarToolbarContribution).toService(NewMiniBrowserOpenHandler);
});

