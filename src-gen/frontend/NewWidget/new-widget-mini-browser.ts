import { injectable, inject, postConstruct } from "inversify";
import { BaseWidget, Navigatable, StatefulWidget, PanelLayout } from "@theia/core/lib/browser";
import URI from '@theia/core/lib/common/uri';

@injectable()
export class NewWidgetMiniBrowserOptions {
  uri: URI;
}
@injectable()
export class NewWidgetMiniBrowser extends BaseWidget implements Navigatable, StatefulWidget {

  static ID = 'new-widget-mini-browser';
  static ICON = 'fa fa-globe';

  @inject(NewWidgetMiniBrowserOptions)
  protected readonly options: NewWidgetMiniBrowserOptions;

  // TODO: to add create content here

  @postConstruct()
  protected init(): void {
    const { uri } = this.options;
    this.id = `${NewWidgetMiniBrowser.ID}:${uri.toString()}`;
    this.title.closable = true;
    this.layout = new PanelLayout({ fitPolicy: 'set-no-constraint'});
  }

  getResourceUri(): URI|undefined {
    return this.options.uri;
  }
  createMoveToUri(resourceUri: URI): URI|undefined {
    return this.options.uri && this.options.uri.withPath(resourceUri.path);
  }

  storeState(): object {
    throw new Error("Method not implemented.");
  }
  restoreState(oldState: object): void {
    throw new Error("Method not implemented.");
  }
}