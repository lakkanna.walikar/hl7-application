import { injectable, inject } from 'inversify';
import { BaseWidget, KeybindingRegistry } from '@theia/core/lib/browser';
import { ILogger } from '@theia/core';
import { WindowService } from '@theia/core/lib/browser/window/window-service';
import { Emitter } from '@theia/core/lib/common/event';

@injectable()
export class NewWidgetBrowserContent extends BaseWidget {

  protected readonly input: HTMLInputElement;
  protected readonly frame: HTMLFrameElement;
  protected loadIndicator: HTMLElement;

  protected readonly naviagateBackEmitter = new Emitter<void>();
  protected readonly openEmitter = new Emitter<void>();

  @inject(ILogger)
  protected readonly logger: ILogger;

  @inject(WindowService)
  protected readonly windowService: WindowService;

  @inject(KeybindingRegistry)
  protected readonly keybindings: KeybindingRegistry;
  
  constructor() {
    super();
    this.node.tabIndex = 0;
    this.input = this.createToolbar(this.node).input;
    const contentArea = this.createContentArea(this.node);
    this.frame = contentArea.frame;
    this.loadIndicator = contentArea.loadIndicator;
    // this.toDispose = []
  }
  
  protected createToolbar(parent: HTMLElement): HTMLDivElement & Readonly <{ input: HTMLInputElement }> {
    const toolbar = document.createElement('div');
    toolbar.classList.add("class-name");
    parent.appendChild(toolbar);
    this.createPrevious(toolbar);
    const input = this.createInput(toolbar);
    input.readOnly = false;
    this.createOpen(toolbar);

    return Object.assign(toolbar, { input });
  }

  protected createPrevious(parent: HTMLElement): HTMLElement {
    return this.onClick(this.createButton(parent, 'Show The previous Page', 'style'), this.naviagateBackEmitter);
  }

  protected createInput(parent: HTMLElement): HTMLInputElement {
    const input = document.createElement('input');
    input.type = 'text';
    parent.appendChild(input);
    return input;
  }

  protected createOpen(parent: HTMLElement): HTMLElement { 
    const button = this.onClick(this.createButton(parent, 'Open In a new window', 'style'), this.openEmitter);
    return button;
  }

  protected createButton(parent: HTMLElement, title: string, ...className: string[]): HTMLElement {
    const button = document.createElement('div');
    button.title = title;
    parent.appendChild(button);
    return button;
  }

  protected onClick(element: HTMLElement, emitter: Emitter<any>): HTMLElement {
    console.log('onClick Content');
    // this.toDispose.push(addEventListener("element", 'click', () => {
    //   if (!element.classList.contains('disabled')) {
    //     emitter.fire(undefined);
    //   }
    // }));
    return element;
  }

  protected createContentArea(parent: HTMLElement): any {
    const contentArea = document.createElement('div');
    contentArea.classList.add("class-name");

    const loadIndicator = document.createElement('div');
    loadIndicator.classList.add("class-name");
    loadIndicator.style.display = 'none';

    const frame = this.createIFrame();
    contentArea.appendChild(loadIndicator);
    contentArea.appendChild(frame);
    parent.appendChild(contentArea);
    return Object.assign(contentArea, { frame, loadIndicator})
  }

  protected createIFrame(): HTMLIFrameElement {
    const frame = document.createElement('iframe');
    // this.toDispos.push(addEventListener(frame, 'load', this.onFrameLoad.bind(this)));
    // this.toDispose.push(addEventListener(frame, 'error', this.onFrameError.bind(this)));
    return frame;
  }
}