import { inject, injectable, postConstruct } from 'inversify';
import { AbstractViewContribution, FrontendApplicationContribution } from '@theia/core/lib/browser';
import { NewWidget } from './NewWidget';
import { ContextKey, ContextKeyService } from '@theia/core/lib/browser/context-key-service';

export const NEW_WIDGET_FACTORY_ID = 'new-widget';

@injectable()
export class NewContrib extends AbstractViewContribution<NewWidget> implements FrontendApplicationContribution {
  
  @inject(ContextKeyService) protected readonly contextKeys: ContextKeyService;

  protected newWidgetFocus: ContextKey<boolean>;

  constructor() {
    super({
      widgetId: NEW_WIDGET_FACTORY_ID,
      widgetName: 'New Widget',
      defaultWidgetOptions: {
        area: 'main',
        rank: 400
      },
      toggleCommandId: 'newWidgetView:toggle',
      // toggleKeybinding: 'ctrlcmd+shift+e'
    });
  }

  @postConstruct()
  protected init(): void {
    this.newWidgetFocus = this.contextKeys.createKey('newWidgetFocus', false);
  }

  async initializeLayout(): Promise<void> {
    console.log('Initialising new widget');
    await this.openView();
  }

  onStart(): void {
    console.log('New Widget started');
  }

}


