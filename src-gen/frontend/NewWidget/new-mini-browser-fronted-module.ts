import { ContainerModule }  from 'inversify';
import { TabBarToolbarContribution } from '@theia/core/lib/browser/shell/tab-bar-toolbar';
import { NewWidgetBrowserContent } from './new-widget-mini-browser-content';
import { NewMiniBrowserOpenHandler } from './new-widget-mini-browser-open-handler';

export default new ContainerModule(bind => {

  bind(NewWidgetBrowserContent).toSelf();
  bind(TabBarToolbarContribution).toService(NewMiniBrowserOpenHandler);
});
