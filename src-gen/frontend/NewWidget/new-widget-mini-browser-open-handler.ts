import { Widget } from '@phosphor/widgets';
import { injectable } from 'inversify';
import URI from '@theia/core/lib/common/uri';
import { MaybePromise } from '@theia/core/lib/common/types';
import { Command, CommandContribution, CommandRegistry } from '@theia/core/lib/common/command';
import { TabBarToolbarContribution, TabBarToolbarRegistry } from '@theia/core/lib/browser/shell/tab-bar-toolbar';
import { NavigatableWidgetOpenHandler} from '@theia/core/lib/browser/navigatable';
import { WidgetOpenerOptions } from '@theia/core/lib/browser/widget-open-handler';
import { NewWidgetMiniBrowser } from './new-widget-mini-browser';

export namespace NewMiniBrowserCommands {
  export const PREV: Command = {
    id: 'new-widget-mini-browser.prev',
    label: 'Open Prev',
    iconClass: 'theia-open-preview-icon'
  };
}

@injectable()
export class NewMiniBrowserOpenHandler extends NavigatableWidgetOpenHandler<NewWidgetMiniBrowser> implements CommandContribution, TabBarToolbarContribution {
  id: string;
  canHandle(uri: URI, options?: WidgetOpenerOptions|undefined): MaybePromise<number> {
    console.log(uri);
    return 1;
    // throw new Error("Method not implemented.");
  }

  registerCommands(commands: CommandRegistry): void {
    commands.registerCommand(NewMiniBrowserCommands.PREV, {
      execute: widget => this.preview(widget),
      isEnabled: widget => this.canPreview(widget),
      isVisible: widget => this.canPreview(widget)
    })
  }

  protected preview(widget?: Widget): void {
    console.log('im in preview');
    return;
  }

  protected canPreview(widget?: Widget): boolean {
    if (widget) {
      console.log('widget exists');
    }
    return true;
  }

  registerToolbarItems(_registry: TabBarToolbarRegistry): void {
    _registry.registerItem({
      id: NewMiniBrowserCommands.PREV.id,
      command: NewMiniBrowserCommands.PREV.id,
      tooltip: 'New Widget Toolbar'
    })
  }
  
  onStart(): void {
    console.log("onStart");
  }
}