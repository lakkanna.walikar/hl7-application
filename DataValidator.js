const Ajv = require('ajv');

const adtA01 = require('./src-gen/schema/json/2.3/ADT_A01.json');
const blocks = require('./src-gen/schema/json/2.3/blocks.json');
const segments = require('./src-gen/schema/json/2.3/segments.json');
const fields = require('./src-gen/schema/json/2.3/fields.json');
const datatypes = require('./src-gen/schema/json/2.3/datatypes.json');

const ajv = new Ajv({schemas: [ adtA01, blocks, segments, fields, datatypes], allErrors: true});

const dataToValidate = [
  // { "EVN.1": 1},
  { //1
    "MSH": {
      "1": "|", "2": "^~\&",
      "3": { "1": "AccMgr" }, "4": { "1": "1" },
      "5": { "1": ""}, "6": { "1": "" },
      "7": "2019-08-13T06:22:31.876Z", "8": "",
      "9": {"1": "ADT", "2": "a01"},
      "10": "599102", "11": { "1": "P"},
      "12": "599102", "13": "2.3",
      "14": "", "15": ""
    }
  },
  { //2
    "EVN": {
      "1": "A01", "2": "20050110045502",
      "3": "", "4": "",
      "5": {}, "6": ""
    }
  },
  { //3
    "PID": {
      "1": 1, // patient id
      "2": {}, // external id
      "3": [ // patient id (internal id)
        {
          "1": "10006579",
          "2": "",
          "3": "",
          "4": { "1": "1" },
          "5": "MRN",
          "6": { "1": "1" }
        },
        {
          "1": "1234567890123456",
          "2": "",
          "3": "",
          "4": { "1": "1" },
          "5": "IHI",
          "6": { "1": "1" }
        }
      ],
      "4": {},  //alternate patient id
      "5": {  // patient name
        "1": "DUCK",
        "2": "DONALD",
        "3": "D"
      },
      "6": {}, // motehr's maiden name
      "7": "19241010",  // date of birth
      "8": "M",  // sex
      "9": [],  // patient alias
      "10": "1", // Race
      "11": [ // patient address
        {
          "1": "111 DUCK ST", // street address
          "2": "", // other designation
          "3": "FOWL",  //city
          "4": "CA", // state or province
          "5": "999990000", // zip or postal code
          "6": "",  // country
          "7": "M"  // Address type
        }
      ],
      "12": "1", // Country
      "13": [{ "1": "8885551212"}],   // phone number - home
      "14": [{"1": "8885551212"}],  // phone number - business
      "15": { "1": "1" },  // primary language
      "16": ["2"],  // marital status
      "17": "", // religion,
      "18": {  // patient account number
        "1": "40007716",
        "2": "",
        "3": "",
        "4": { "1": "AccMgr" },
        "5": "VN",
        "6": { "1": "1" }
      },
      "19": "123121234",  // ssn number - patient
      "30": "NO"    // patient death indicator
    }
  },
  { "PD1": {

  }}, // 4
  {
    "NK1": {
      "1": 1,
      "7": {}
    }
  }, // 5
  {
    "PV1": {

    }
  }, // 6
  {
    "PV2": {

    }
  }, // 7
  {
    "DB1": {
      "1": 12
    }
  }, // 8 DB1
  {
    "OBX": {
      "2": "",
      "3": {},
      "11": ""
    }
  }, // 9
  {
    "AL1": {
      "1": 12,
      "3": 12
    }
  }, // 10 AL1
  {
    "DG1": {
      "1": 12,
      "6": "12"
    }
  }, // 11 DG1
  {
    "DRG": {
      "1": {}
    }
  }, // 12 DRG
  {
    "PROC": [
      {
        "PR1": {
          "1": 1,
          "2": "",
          "6": ""
        },
        "ROL": [
          {
           "1": {},
           "2": "12",
           "4": {}
          }
        ]
      }
    ]
  }, // 13
  {
    "GT1": {
      "1": 123,
      "3": [
        { "1": "KJFDSK"}
      ]
    }
  }, // 14 GT1
  {
    "IN": [
      {
        "IN1": {
          "1": 1,
          "3": {}
        }
      },
      {
        "IN1": {
          "1": 1,
          "3": {}
        },
        "IN3": {
          "1": 3
        }
      }
    ]
  },
  {
    "ACC": {
      "1": "2019-09-09"
    }
  },
  {
    "UB1": {
      "1": 123
    }
  }, // UB1
  {
    "UB2": {
      "1": 123
    }
  }, // UB2
];

const schemaID = {
  adtA01: "https://314e.com/schemas/v2.3/adt-a01.json"
}

class Validator {
  constructor(schemaFor = "adtA01") {
    this.validate = ajv.getSchema(schemaID[schemaFor]);
  }

  checkValidation(data = dataToValidate) {
    const valid = this.validate(data);
    if (valid) {
      console.log("😎 VALIDATED SUCCESSFULLY 😎");
    } else {
      console.log("#######    ERROR'S    #######");
      this.validate.errors.forEach((error) => {
        console.error(`🚫 ${error.dataPath} -> ${error.message}`);
      });
    }
  }
}

module.exports = Validator;
