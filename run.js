const fs = require("fs");
const Validator = require("./DataValidator.js");

const BASE_SCHEMA_PATH = "./src-gen/schema/json";

class MessageValidator {
  constructor(filePath = "./src-gen/samples/ADT_A01.hl7") {
    this.filePath = filePath;
    this.errorReadingFile = false;
    this.fileData = [];
    this.mshIndex = [];
    this._messages = [];
    this._messageObject = [];
    this.readFile();
  }

  readFile() {
    fs.readFile(this.filePath, (error, data) => {
      if (error) {
        this.errorReadingFile = true;
        console.error(error);
      } else {
        this.fileData = data.toString().split("\n");
        this.findMSH();
      }
    });
  }

  findMSH() {
    this.fileData.forEach((line, index) => {
      if (line.startsWith("MSH")) {
        this.mshIndex.push(index);
      }
      if (index === this.fileData.length - 1) {
        this.findMessages();
      }
    });
  }

  findMessages() {
   this.mshIndex.forEach((lineIndex, index) => {
      let endIndex = index + 1;
      if (index === this.mshIndex.length - 1) {
        endIndex = this.fileData.length;
        const message = this.fileData.slice(lineIndex, endIndex);
        this._messages.push(message);
        this.generateDataObject();
      } else {
        const message = this.fileData.slice(lineIndex, endIndex);
        this._messages.push(message);
      }
    });
  }

  validate (data) {
    const validator = new Validator();
    validator.checkValidation(data);
    // validator.checkValidation();
  }

  loadSchema(version, messageType) {
    const basechema = `${BASE_SCHEMA_PATH}/${version}/${messageType}.json`;
    const segments = `${BASE_SCHEMA_PATH}/${version}/segments.json`;
    const fields = `${BASE_SCHEMA_PATH}/${version}/fields.json`;
    const dataTypes = `${BASE_SCHEMA_PATH}/${version}/datatypes.json`;

    return {
      baseSchema: require(basechema),
      segmentSchema: require(segments),
      fieldSchema: require(fields),
      dataTypeSchema: require(dataTypes)
    };
  }

  getFieldSchema(fieldSchema, segment, field) {
    const seg = fieldSchema.definitions[segment];
    switch (seg.type) {
      case 'object':
        console.log("seg:   ", seg.properties[field]);
        break;

      default:
        break;
    }
  }

  static separateComponents(dataToSeparate, separator) {
    const data = {};
    dataToSeparate.split(separator).forEach((value, idx) => {
      data[idx + 1] = value;
    });
    return data;
  }

  static findTypeOfSeparator(data, separators) {
    let choice = "default";
    separators.forEach((separator) => {
      if (data.includes(separator)) {
        choice = separator;
      }
    });
    return choice;
  }

  // static func(dateToSeparate, resultObj, i, separators, choice) {
  //   const [ componentSeparator, fieldRepeatSeparator, subComponentSeparator ] = separators;
  //   switch(choice) {
  //     case componentSeparator:
  //       return MessageValidator.separateComponents(dataToSeparate, componentSeparator);
  //     case fieldRepeatSeparator:

  //   }
  // }

  static iteratorFields(dataToIterate, resultObj, i, separators) {
    let choice = "default";
    const [ componentSeparator, fieldRepeatSeparator, subComponentSeparator ] = separators;

    choice =  this.findTypeOfSeparator(dataToIterate, separators);

    switch (choice) {
      case componentSeparator:
        // console.log("component: ", dataToIterate)
        resultObj[i] = MessageValidator.separateComponents(dataToIterate, componentSeparator);
        break;

      case fieldRepeatSeparator:
        // console.log("field repeat: ", dataToIterate)

          dataToIterate.split(fieldRepeatSeparator).forEach((component) => {
              if (Array.isArray(resultObj[i])) {
                resultObj[i].push(MessageValidator.separateComponents(component, componentSeparator));
              } else {
                resultObj[i] = [{
                  ...resultObj[i],
                  ...MessageValidator.separateComponents(component, componentSeparator)
                }];
              }
          });
        break;

      case subComponentSeparator:
          dataToIterate.split(componentSeparator).forEach((component, dx) => {
            resultObj[i] = {
              ...resultObj[i],
              [dx + 1]: component
            }
          });
        break;

      default:
        resultObj[i] = dataToIterate;
        break;
    }
  }

  generateDataObject(messages = this._messages) {
    messages.forEach((message) => {
      const [ fieldSeparator, componentSeparator,
        fieldRepeatSeparator, escapeCharacter, subComponentSeparator ] = message[0].slice(3, 8);
      let messageType = "";
      let version = "";

      message.forEach((segment, segIndex) => {
        if (segment) {

          let fields = segment.split(fieldSeparator);

          if (segIndex === 0) {
            const [msgType, triggerEvent] = fields[8].split(componentSeparator);
            messageType  = `${msgType}_${triggerEvent}`;
            version = fields[11];
            const { baseSchema, segmentSchema, fieldSchema, dataTypeSchema } = this.loadSchema(version, messageType);
            // this.getFieldSchema(fieldSchema, "MSH", 3);
            // const m = baseSchema.items.filter(x => x.title === "Message Header");
            // console.log("m: ", m)
          }

          let msg = {};
          const segName = fields.shift();
          fields.forEach((field, fieldIndex) => {

            if (fieldIndex === 0 && segIndex === 0) {
              msg[fieldIndex + 1] = fieldSeparator;
              msg[fieldIndex + 2] = field;
              return;
            } else {
              let fldIdx = segIndex === 0 ? fieldIndex + 2 : fieldIndex + 1;
              MessageValidator.iteratorFields(field, msg, fldIdx, [componentSeparator, fieldRepeatSeparator, subComponentSeparator]);
            }

          });

          this._messageObject.push({
            [segName]: {
              ...msg
            }
          });
        }
      });
    });
    console.log("converted message data object: \n");
    // console.log(this._messageObject);
    console.log(this._messageObject);
    console.log("\n");
    // this.validate(this._messageObject);
  }

}

const msgValidator = new MessageValidator();
// msgValidator.validate();
// msgValidator.generateDataObject();
