# hl7-application
## setup

requirements:
```
node: 10.16.0
```
```
git clone https://gitlab.com/lakkanna.walikar/hl7-application.git
```
```
nvm use v10.16.0
```
```
yarn
```
```
  yarn rebuild:electron

  OR

  yarn rebuild:browser
```

### If Error
```
yarn build:theia

// revert changes in src-gen/frontend/index.js

// in webpack.config -> rules:
    {
      test: /\.tsx?$/,
      use: 'ts-loader',
      exclude: /node_modules/
    }

// in webpack.config -> replace extensions (resolve)
  
  extensions: ['.ts', '.tsx', '.js'],
```

```
yarn build
```

```
yarn start
```

## if node-gyp error
1. In root directory of project
    ```
    wget https://nodejs.org/download/release/v10.16.0/node-v10.16.0-headers.tar.gz)
    ```
2. run
    ```
    npm_config_tarball=/path/to/node-v10.16.0-headers.tar.gz yarn install
    ```
    **/path/to/**  -> current project path (ex: run **pwd** in terminal to get path)

## Note: For more information https://www.theia-ide.org/doc/composing_applications